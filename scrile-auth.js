// ==UserScript==
// @name         Auth for Scrile websites
// @namespace    http://tampermonkey.net/
// @version      0.1.4
// @description  Automatically substitute password
// @author       Backend
// @match      https://*/admin
// @match      https://*/admin/login
// @match      https://*/mielpotadmin1221
// @match      https://*/mielpotadmin1221/login
//
// @icon       data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @downloadURL https://gitlab.com/borisov0alex/userscripts/-/raw/master/scrile-auth.js
// @updateURL https://gitlab.com/borisov0alex/userscripts/-/raw/master/scrile-auth.js
// @grant      GM_xmlhttpRequest
// @grant      GM_openInTab
// @run-at document-idle
// @connect auth.cluster.softservice.org
// ==/UserScript==

// cat .production-gitlab-ci.yml | grep '(?<=DOMAIN_NAMES: ").+(?=")(?<!connect.scrile.com)' -oP | sed 's/www.//g' | tr ' ' "\n" | xargs -i echo -e '// @match      https://*.{}/admin/login\n// @match      https://*.{}/admin'

(function() {
    'use strict';
	console.log('Substituting password');
    var passwordCheckTimeout = 2000;
    var inputCheckTimeout = 1100;
    var checkFunc = () => {
        if (document.querySelectorAll("form").length == 1 && document.querySelector('input[name=email]') && document.querySelector('input[type=password]')) {
            GM_xmlhttpRequest({'method': 'get', 'url': 'https://auth.cluster.softservice.org/code', 'onerror': () => {
                GM_openInTab('https://auth.cluster.softservice.org', {incognito: false});
            }, 'onload': (data) => {
                try {
                    if (document.querySelectorAll("form").length == 1 && document.querySelector('input[name=email]') && document.querySelector('input[type=password]')) {
                        document.querySelector('input[name=email]').value = 'admin@scrile.com';
                        document.querySelector('input[name=email]').dispatchEvent(new Event('input', {bubbles:true}));
                        document.querySelector('input[type=password]').value = JSON.parse(data.responseText).password;
                        document.querySelector('input[type=password]').dispatchEvent(new Event('input', {bubbles:true}));
                        window.setTimeout(checkFunc, passwordCheckTimeout);
                    } else {
                        window.setTimeout(checkFunc, inputCheckTimeout);
                    }
                } catch (e) {console.error(e); }
            }});
        } else {
            window.setTimeout(checkFunc, inputCheckTimeout);
        }
    }
    checkFunc();
})();
